<?php
namespace Patryk\TestBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Form class
 */
class ItemForm extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('textfield', 'text')
            ->add('datefield', 'collot_datetime', array(
                'pickerOptions' =>
                    array('format' => 'mm/dd/yyyy',
                        'weekStart' => 1,
                        'startDate' => '01/01/1900',
                        'endDate' => '12/31/2100',
                        'autoclose' => true,
                        'startView' => 'month',
                        'minView' => 'month',
                        'maxView' => 'month',
                        'todayBtn' => false,
                        'todayHighlight' => true,
                        'language' => 'en',
                        'forceParse' => true,
                        'pickerPosition' => 'bottom-right',
                        'viewSelect' => 'month',
                        'showMeridian' => false,
                        'initialDate' => date('m/d/Y'),
                    )
            ))
            ->add('imagefield', 'file')
            ->add('save', 'submit');
    }

    public function getName() {
        return 'ItemForm';
    }
}
