<?php

namespace Patryk\TestBundle\Entity;

/**
 * Item class
 */
class Item
{
    /**
     * @var integer

     */
    private $id;

    /**
     * @var string
     */
    private $textfield;

    /**
     * @var \DateTime
     */
    private $datefield;

    /**
     * @var string
     */
    private $imagefield;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set textfield
     *
     * @param string $textfield
     * @return Item
     */
    public function setTextfield($textfield)
    {
        $this->textfield = $textfield;

        return $this;
    }

    /**
     * Get textfield
     *
     * @return string
     */
    public function getTextfield()
    {
        return $this->textfield;
    }

    /**
     * Set datefield
     *
     * @param \DateTime $datefield
     * @return Item
     */
    public function setDatefield($datefield)
    {
        $this->datefield = $datefield;

        return $this;
    }

    /**
     * Get datefield
     *
     * @return \DateTime
     */
    public function getDatefield()
    {
        return $this->datefield;
    }

    /**
     * Set imagefield
     *
     * @param string $imagefield
     * @return Item
     */
    public function setImagefield($imagefield)
    {
        $this->imagefield = $imagefield;

        return $this;
    }

    /**
     * Get imagefield
     *
     * @return string
     */
    public function getImagefield()
    {
        return $this->imagefield;
    }
}
