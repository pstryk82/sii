<?php

namespace Patryk\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Patryk\TestBundle\Entity\Item;
use Patryk\TestBundle\Form\ItemForm;

class DefaultController extends Controller {

    /**
     * @var string
     */
    private $formTemplatePath = 'PatrykTestBundle:Default:index.html.twig';

    /**
     * @var string
     */
    private $successTemplatePath = 'PatrykTestBundle:Default:success.html.twig';

    /**
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return Symfony\Component\HttpFoundation\Response;
     */
    public function indexAction(Request $request) {
        $item = new Item();
        $form = $this->createForm(new ItemForm(), $item);

        $form->handleRequest($request);

        if ($form->isValid()) {
            return $this->redirect($this->generateUrl('patryk_test_success'));
        }
        return $this->render(
            $this->formTemplatePath,
            array(
                'form' => $form->createView()
            )
        );
    }

    /**
     *
     * @return Symfony\Component\HttpFoundation\Response;
     */
    public function successAction() {
        return $this->render(
            $this->successTemplatePath
        );
    }
}
